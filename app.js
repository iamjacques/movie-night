const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const config = require('config');
const debug = require('debug')('app:all');
const webRouter = require('./routes/web');
const apiRouter = require('./routes/api');
const bootstrap = require('./bootstrap/app');

//Config
const app = express();

app.use(helmet())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 
    extended: true
}));
app.use(express.static('public'));
app.set('view engine', 'pug');
bootstrap.init();

// Web Routes
app.use('/', webRouter);
app.use('/movies', webRouter);

// Api Routes
app.use('/v1/', apiRouter);


const port = process.env.NODE_PORT || 8081;
app.listen(port, () => console.log(`Listening on port ${port}...`))