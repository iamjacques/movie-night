const Joi = require('joi');

exports.MovieValidator = (movie) => {
    const Schema = {
        name: Joi.string().min(2).required(),
        author: Joi.string().min(2).required(),
        tags:  Joi.array()
    };
    return Joi.validate(movie, Schema);
}

exports.UserValidator = (movie) => {
    const Schema = {
        first_name: Joi.string().min(2).required(),
        last_name: Joi.string().min(2).required(),
        country: Joi.string(),
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(2).required(),
        profile_image: Joi.string(),
        preferred_genres: Joi.array()
    };
    return Joi.validate(movie, Schema);
}