const mongoose = require('mongoose');

exports.init = () => {
    mongoose.connect('mongodb://localhost/movie-night',  { useNewUrlParser: true} )
            .then(() => console.log('connected to mongodb'))
            .catch((err) => console.log('Failed to connect to mongodb', err))
}