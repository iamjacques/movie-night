const express = require('express');
const router = express.Router();

const {
    TVSeriesCtrl,
    MoviesCtrl
} = require('../../app/http/controllers')



router.get('/', TVSeriesCtrl.index);
router.get('/movies', MoviesCtrl.index);
// router.use('/', TVSeriesCtrl.index);
// router.use('/', TVSeriesCtrl.index);



module.exports = router;