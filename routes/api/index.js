const express = require('express');
const router = express.Router();

const { MoviesApiCtrl, TVSeriesApiCtrl, UserApiCtrl  } = require('../../app/http/controllers')

// Movies 
router.get('/movies', MoviesApiCtrl.findAll);
router.get('/movies/:id', MoviesApiCtrl.read);
//router.post('/movies', MoviesApiCtrl.create);
router.put('/movies/:id', MoviesApiCtrl.update);
router.delete('/movies/:id', MoviesApiCtrl.delete);
router.get('/import/movies', MoviesApiCtrl.import);

// TV Series
router.get('/series', TVSeriesApiCtrl.findAll);
router.get('/series/:id', TVSeriesApiCtrl.read);
// router.post('/series', TVSeriesApiCtrl.create);
router.put('/series/:id', TVSeriesApiCtrl.update);
router.delete('/series/:id', TVSeriesApiCtrl.delete);
router.get('/import/tv', TVSeriesApiCtrl.import);

// User
router.get('/user/:id', UserApiCtrl.read);
router.post('/user', UserApiCtrl.create);
router.put('/user/:id', UserApiCtrl.update);

module.exports = router;

