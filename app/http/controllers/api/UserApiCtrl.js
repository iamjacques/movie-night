const User = require('../../../models/User');
const { UserValidator } = require('../../../../helpers/Validators');

exports.read = async (req, res) => {
    if (!req.params.id) return res.status(404).send(`Movie id is required`);

    const user = await User.findById(req.params.id, (error, user) => {
        if (!user) return res.status(404).send(`Movie not found`);
    })  

    res.status(200).json({
        user
    });
}

exports.create = async (req, res) => {

    const { error } = UserValidator(req.body);
    if (error) return res.status('400').send({message: error.details[0].message });

    const userModel = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        country: req.body.country,
        email: req.body.email,
        password: req.body.password,
        profile_image: req.body.profile_image,
        preferred_genres: req.body.preferred_genres
    });

    const user = await userModel.save();

    res.status(200).json({
        success: true,
        user
    });
}

exports.update = async (req, res) => {
    const movie = await Movie.findOneAndUpdate({
        _id: req.params.id
    }, {
        $set: req.body
    }, {
        new: true
    });

    res.status(200).json({
        movie
    });
}