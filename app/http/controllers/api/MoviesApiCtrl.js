const axios = require('axios');

const Movie = require('../../../models/Movies');
const { MovieValidator } = require('../../../../helpers/Validators');
const config = require('config')
const apiUrl = config.get('themoviedb.url')
const apiToken = config.get('themoviedb.token')

exports.findAll = async (req, res) => {
    const p = 1, s = 2;
        
    const movies = await Movie.find().skip((p - 1) * s).limit(p);
    if (!movies) return res.status(404).send(`No movies found`);

    res.status(200).json({
        movies
    });
}

exports.read = async (req, res) => {
    if (!req.params.id) return res.status(404).send(`Movie id is required`);

    const movie = await Movie.findById(req.params.id, (error, movie) => {
        if (!movie) return res.status(404).send(`Movie not found`);
    })  

    res.status(200).json({
        movie
    });
}

const create = async (movieData) => {
    const movie =  await Movie.find({ title : movieData.title }).then((err, m) => {
        if (movie){
            console.log('here', movie )
            return null;
        }
    });
}

exports.update = async (req, res) => {
    const movie = await Movie.findOneAndUpdate({
        _id: req.params.id
    }, {
        $set: req.body
    }, {
        new: true
    });

    res.status(200).json({
        movie
    });
}

exports.delete = async (req, res) => {
    if (!req.params.id) return res.status(404).send(`Course id is required`);
    const movie = await Movie.remove({
        _id: req.params.id
    });

    res.status(200).json({
        success: true,
        message: 'Movie deleted: ' + req.params.id
    });
}

exports.import = async (req, res) => {

    const url = '/4/discover/movie?sort_by=popularity.desc'; 
    
    try { 
        const { data } = await axios.get(apiUrl+url,  { headers: { Authorization: 'Bearer ' + apiToken } } )
        
        res.status(200).json({ success: true, data });
    } catch (error) {
        res.status(401).json({
            success: false,
            message: error.message
        })
    }
}
