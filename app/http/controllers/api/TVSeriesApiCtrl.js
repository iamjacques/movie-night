
const TVSerie = require('../../../models/TVSeries');
const config = require('config')
const apiUrl = config.get('themoviedb.url')
const apiToken = config.get('themoviedb.token')


exports.findAll = (req, res) => {
    res.status(200).json({
        page: 'All Series'
    });
}

exports.read = (req, res) => {
    res.status(200).json({
        action: 'Read Series: '+req.params.id
    });
}

exports.create = (req, res) => {
    res.status(200).json({
        action: 'Create Series'
    });
}

exports.update = (req, res) => {
    res.status(200).json({
        action: 'Update Series: '+req.params.id
    });
}

exports.delete = (req, res) => {
    res.status(200).json({
        action: 'Delete Series: '+req.params.id
    });
}

exports.import = async (req, res) => {

    const url = '/4/discover/tv?sort_by=popularity.desc';
    try { 
        const { data } = await axios.get(apiUrl+url,  { headers: { Authorization: 'Bearer ' + apiToken } } )
        
        const imported = data.results.map( async (item) => {
                const movies = new Movie(item); 
                const newMovie = await movies.save();
                return newMovie;
          })

        res.status(200).json({ success: true, imported });
    } catch (error) {
        res.status(401).json({
            success: false,
            message: error.message
        })
    }
}