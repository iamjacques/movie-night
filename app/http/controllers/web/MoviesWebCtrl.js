
const config = require('config');

const MoviesCtrl = {
    index: (req, res) => {
        res.render('movies', {
            title: config.get('app_name'),
            page: 'Movies'
        });
    }
}

module.exports = MoviesCtrl;