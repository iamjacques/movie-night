const config = require('config');

const TVSeriesCtrl = {
    index: (req, res) =>{
         res.render('series', {
             title: config.get('app_name'),
             page: 'TV Series'
         });
    }
}

module.exports = TVSeriesCtrl;