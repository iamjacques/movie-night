module.exports = {
    TVSeriesCtrl: require('./web/TVSeriesWebCtrl'),
    MoviesCtrl: require('./web/MoviesWebCtrl'),

    MoviesApiCtrl: require('./api/MoviesApiCtrl'),
    TVSeriesApiCtrl: require('./api/TVSeriesApiCtrl'),
    UserApiCtrl: require('./api/UserApiCtrl'),
    
}