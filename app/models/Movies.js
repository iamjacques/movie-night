const mongoose = require('mongoose');

const MovieSchema = new mongoose.Schema({
    popularity:  mongoose.Schema.Types.Decimal128,
    vote_count: Number,
    video: Boolean,
    poster_path: String,
    id: Number,
    adult: Boolean,
    backdrop_path: String,
    original_language: String,
    original_title: String,
    genre_ids:  [ Number   ],
    title: String,
    vote_average:  mongoose.Schema.Types.Decimal128,
    overview: String,
    release_date:  {type: Date }
})

const MoviesModel = mongoose.model('Movies', MovieSchema);

module.exports = MoviesModel;