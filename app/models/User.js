const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;



const UserSchema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    country: String,
    email: { type: String, index: true, unique: true },
    password: String,
    profile_image: String,
    preferred_genres: Array,
    meta: [ Object ],
    active: { type: Boolean, default: false },
    date: { type: Date, default: Date.now() } 
})

UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

const UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;